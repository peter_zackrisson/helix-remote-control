# Helix Remote Control #

This project combines the love for music, programming, electronics and 3d-modelling into a multi-functional wireless remote control for you Helix

![FS](./pictures/with_strap.jpg)

### Functions ###

The software so far supports:

* Control 6 foot switches (5 for HX Stomp)
* Select between 6 snapshots (3 for HX Stomp)
* Control looper (Play/Stop, Record/Overdub, Undo/Redo, Full/Half speed, Forward/Reverse. Especially useful for HX Stomp to select half speed and reverse mode)
* Bring up tuner and control tap tempo from remote. Frees up one foot switch for HX stomp.
* Step between presets (+/- or step 01A to 02A etc.). Also shows current preset on screen and can jump to preset 01A.
* Send function for a midi cc. Demo includes for playing canon by Pachelbel and  controlling wah pedal using complex function (sin(2 \* x \* x \* PI / 10000) + 1) \* 127 / 4 + 50. See my [helix-midi-function-generator](https://bitbucket.org/peter_zackrisson/helix-midi-function-generator) project for a java program that can show and generate functions and send using midi cc.
* Instead of toggle between views with view button, it's now possible to jump directly to a certain view by first holding down view and then press 1: FS, 2: SNAPSHOT, 3: PRESET, 4: SET, 5: FUNCTION, 6: LOOPER
* Reset by holding down view button for 3 seconds (to enter special view) and then press 1.

HX Stomp mode is selected by either hold down view button for 3s and press 6 or by changing preset on your HX Stomp (it detects Helix not sending any midi cc together with midi pc as done for helix floor at preset changes). In this mode the snapshot page can also control foot switches 1 to 3.

![FS](./pictures/fs_floor.jpg)
![FS](./pictures/snapshot_floor.jpg)
![FS](./pictures/set_floor.jpg)
![FS](./pictures/looper.jpg)
![FS](./pictures/function.jpg)

### Demo preset (floor) ###

[Miditest.hlx](./presets/Miditest.hlx)

### Parts needed ###

* [Widi Master from CME](https://www.cme-pro.com/widi-master/)
* [Lora 32](https://heltec.org/project/wifi-lora-32/) or similar supporting [BLE-MIDI](https://www.midi.org/specifications-old/item/bluetooth-le-midi). The Lora part is not used. I just wanted a device that had a small screen and supports BLE-MIDI.
* 7 buttons (bought a cheap box with many different small buttons on wish.com)
* Thin wire (thin enough to go through hole with coating on)
* Nano tape or similar to stick remote control to guitar
* Re-chargeable Battery. I currently use a USB power bank sewn into guitar strap with an elastic band. The Lora board supports battery management if you want to use e.g. a 18650 battery.
* 3D-printer

![FS](./pictures/helix_floor.jpg)
![FS](./pictures/battery.jpg)
![FS](./pictures/battery_and_cables.jpg)
![FS](./pictures/cables_back.jpg)
![FS](./pictures/lora_side.jpg)  
[3D-model](./3d/HelixRemotev14.f3z) Fusion 360 model

### How to build software ###

* Code is compiled using standard Arduino IDE. Additional boards url: https://arduino.esp8266.com/stable/package_esp8266com_index.json,https://dl.espressif.com/dl/package_esp32_index.json
* (Eclipse IDE used as editor)

### Contact ###

If you find this useful, if you want to suggest improvement or add new cool functions, please send an email to

![mail](./pictures/Mail.png)

### TODO ###

* Make some demo videos
* Handle roll-over at preset change
* Add graphic symbols in looper view
* Figure out why pressing button 5 makes screen brighter
* Add support for 2 more buttons
* Improve 3d-model. There is a scaling issue.
* Support reading Variax volume and tone control
