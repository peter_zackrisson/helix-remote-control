/*
 * Program is made by Peter Zackrisson
 *
 * Feel free to use and modify this code but also feel free to send me a message if you find it useful.
 *
 *
 * Floor at preset change:
 * 12:21:49.333 -> OnControlChange, channel:1 ,number: 0 ,value: 0
 * 12:21:49.333 -> OnControlChange, channel:1 ,number: 32 ,value: 3
 * 12:21:49.333 -> OnProgramChange, channel:1 ,number: 21
 *
 *
 * HX stomp just sends Program change 0-125
 *
 */

#include <BLEMIDI_Transport.h>

// THIS WORKS WITH WIDI MASTER!!!
#include <hardware/BLEMIDI_ESP32_NimBLE.h>

#include "helix_midi.h"
#include "limits.h"

//Libraries for LoRa
//#include <SPI.h>

//Libraries for OLED Display
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

//OLED pins
#define OLED_SDA 4
#define OLED_SCL 15
#define OLED_RST 16
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

BLEMIDI_CREATE_DEFAULT_INSTANCE()

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RST);

void (*resetFunc)(void) = 0; //declare reset function at address 0

/*
 * Button definitions
 */
const int VIEW_BUTTON = 13;
const int BUTTON1 = 23;
const int BUTTON2 = 22; //
const int BUTTON3 = 17;
const int BUTTON4 = 12; //18 not working
const int BUTTON5 = 21; //19 sometimes working, 26 not work
const int BUTTON6 = 5;

#define DEBOUNCE_TIME 20 // ms
#define PRESET_PER_BANK_FLOOR 4
#define PRESET_PER_BANK_STOMP 3

unsigned long t0 = millis();
unsigned long function2nextNoteTime;
unsigned long viewButtonPressedTime = LONG_MAX;
bool isConnected = false;
bool wasConnected = false;
int currentProgram = 0; // Deafult preset
bool functionGenerator = false;
bool functionGenerator2 = false;
bool functionGenerator3 = false;
bool functionGenerator4 = false;
bool button1LooperState = false;
bool button3looperState = false;
bool button4looperState = false;
bool button5looperState = false;
bool button6looperState = false;
bool hxStompMode = false;
bool controlChangeReceived = false;
int viewButtonValue = 0;
int viewButtonPrev = 0;
int button1value = 0;
int button1valuePrev = 0;
int button2value = 0;
int button2valuePrev = 0;
int button3value = 0;
int button3valuePrev = 0;
int button4value = 0;
int button4valuePrev = 0;
int button5value = 0;
int button5valuePrev = 0;
int button6value = 0;
int button6valuePrev = 0;
bool anyChangeMade = false;

int x = 0;
int x2 = 0;
int x4 = 0;
long lastFunctionTime = millis();
long lastFunctionTime3 = millis();
long lastFunctionTime4 = millis();


/*
 * MIDI definitions
 */
const int MIDI_CHANNEL = 1;

const int FS_VIEW = 0;
const int SNAPSHOT_VIEW = 1;
const int PRESET_VIEW = 2;
const int SET_VIEW = 3;
const int FUNCTION_VIEW = 4;
const int LOOPER_VIEW = 5;
const int SPECIAL_VIEW = 99;
const int NUMBER_OF_VIEWS = 6;
int currentView = FS_VIEW;

String stateStrings[NUMBER_OF_VIEWS][3] = { //
        { "<FS>",                           //
                " 1  2  3",                 //
                " 4  5" },                  // Draw last 7 only for HX floor
                { "<SNAPSHOT>",             //
                        " 1  2  3",         //
                        " 4  5  6" },       //
                { "<PRESET>",               //
                        "UP +  >01A",       //
                        "DN -" },           //
                { "<SET>",                  //
                        " 1  2  3",         //
                        " 4  5  6" },       //
                { "<FUNCTION>",             //
                        "WA CA TU",         //
                        "SI PU TA" },       //
                { "<LOOPER>",               //
                        "UN ON HA",         //
                        "RE PL RV" }        //
        };                                  //

String A2D[4] = { "A", "B", "C", "D" };

int canon_bpm = 100;
int canon_note_position = 0; // 0-7, 8 notes
#define canon_number_of_notes 8
int canon[canon_number_of_notes][3] = { //
        { //
                MIDI_3_NOTE_OSCILLATOR_G, MIDI_3_NOTE_OSCILLATOR_OCTAVE_3, HALF_NOTE },   //
                { MIDI_3_NOTE_OSCILLATOR_D, MIDI_3_NOTE_OSCILLATOR_OCTAVE_3, HALF_NOTE }, //
                { MIDI_3_NOTE_OSCILLATOR_E, MIDI_3_NOTE_OSCILLATOR_OCTAVE_3, HALF_NOTE }, //
                { MIDI_3_NOTE_OSCILLATOR_B, MIDI_3_NOTE_OSCILLATOR_OCTAVE_2, HALF_NOTE }, //
                { MIDI_3_NOTE_OSCILLATOR_C, MIDI_3_NOTE_OSCILLATOR_OCTAVE_3, HALF_NOTE }, //
                { MIDI_3_NOTE_OSCILLATOR_G, MIDI_3_NOTE_OSCILLATOR_OCTAVE_2, HALF_NOTE }, //
                { MIDI_3_NOTE_OSCILLATOR_C, MIDI_3_NOTE_OSCILLATOR_OCTAVE_3, HALF_NOTE }, //
                { MIDI_3_NOTE_OSCILLATOR_D, MIDI_3_NOTE_OSCILLATOR_OCTAVE_3, HALF_NOTE }, //
        };

// -----------------------------------------------------------------------------
// SETUP
// -----------------------------------------------------------------------------
void setup() {
    // Initialize Serial Monitor
    Serial.begin(115200);
    Serial.println("setup");
    Serial.println(LED_BUILTIN);

    MIDI.begin();

    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);

    pinMode(VIEW_BUTTON, INPUT_PULLUP);
    pinMode(BUTTON1, INPUT_PULLUP);
    pinMode(BUTTON2, INPUT_PULLUP);
    pinMode(BUTTON3, INPUT_PULLUP);
    pinMode(BUTTON4, INPUT_PULLUP);
    pinMode(BUTTON5, INPUT_PULLUP);
    pinMode(BUTTON6, INPUT_PULLUP);

    BLEMIDI.setHandleConnected(OnConnected);
    BLEMIDI.setHandleDisconnected(OnDisconnected);

    //MIDI.setHandleNoteOn(OnNoteOn);
    //MIDI.setHandleNoteOff(OnNoteOff);
    MIDI.setHandleProgramChange(OnProgramChange);
    MIDI.setHandleControlChange(OnControlChange);

    //MIDI.setHandleClock(OnClock);
    //MIDI.setHandleStart(OnStart);
    //MIDI.setHandleStart(OnStop);
    //MIDI.setHandleContinue(OnContinue);
    //MIDI.setHandleActiveSensing(OnActiveSensing);
    //MIDI.setHandleSystemReset(OnSystemReset);
    //MIDI.setHandleTuneRequest(OnTuneRequest);

    //MIDI.setHandleAfterTouchPoly(OnAfterTouchPoly);
    //MIDI.setHandleAfterTouchChannel(OnAfterTouchChannel);
    //MIDI.setHandlePitchBend(OnPitchBend);
    //MIDI.setHandleSystemExclusive(OnSystemExclusive);
    //MIDI.setHandleTimeCodeQuarterFrame(OnTimeCodeQuarterFrame);
    //MIDI.setHandleSongPosition(OnSongPosition);
    //MIDI.setHandleSongSelect(OnSongSelect);

    //reset OLED display via software
    pinMode(OLED_RST, OUTPUT);
    digitalWrite(OLED_RST, LOW);
    delay(20);
    digitalWrite(OLED_RST, HIGH);

    // Initialize OLED
    Wire.begin(OLED_SDA, OLED_SCL);
    if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3c, false, false)) { // Address 0x3C for 128x32
        Serial.println(F("SSD1306 allocation failed"));
        for (;;)
            ; // Don't proceed, loop forever
    }

    display.clearDisplay();
    display.setTextColor(WHITE);
    display.setTextSize(2);
    display.setCursor(0, 0);
    display.print("SETUP");
    display.setCursor(0, 20);
    display.print("Searching");
    display.setCursor(0, 40);
    display.print("for Midi");
    display.display();
}

String getCurrentProgram() {
    int presetsPerBank = 4; // Default for floow
    if (hxStompMode) {
        presetsPerBank = 3;
    }

    String prg = String((currentProgram / presetsPerBank) + 1); // 01C
    int atod = (currentProgram % presetsPerBank);
    if (currentProgram < 32) {
        prg = "0" + prg;
    }
    prg = prg + A2D[atod];
    return prg;
}

void drawScreen() {
    display.clearDisplay();
    display.setTextColor(WHITE);
    display.setCursor(0, 0);

    if (currentView == SPECIAL_VIEW) {
        display.setTextSize(1);
        display.print("1 = Reset");
        display.setCursor(0, 20);
        display.print("3 = Exit");
        display.setCursor(0, 40);
        display.print("6 = Toggle stomp");
    } else {
        display.setTextSize(2);
        display.print(stateStrings[currentView][0]);
        display.setCursor(0, 20);
        display.print(stateStrings[currentView][1]);
        display.setCursor(0, 40);
        if ((currentView == SNAPSHOT_VIEW) && hxStompMode) {
            display.print(stateStrings[currentView][1]);
            display.setTextSize(1);
            display.setCursor(110, 40); // 110 is guessing
            display.print("FS");
            display.setTextSize(2);
        } else {
            display.print(stateStrings[currentView][2]);
        }

        if (currentView == PRESET_VIEW) {
            display.setCursor(115, 0);
            if (hxStompMode) {
                display.print("S");
            } else {
                display.print("F");
            }

            display.setCursor(65, 40);
            display.print(getCurrentProgram());
        }

        if (currentView == FS_VIEW) {
            if (!hxStompMode) {
                display.setCursor(84, 40);
                display.print("7");
            }
        }

        if (currentView == FUNCTION_VIEW) {
            if (functionGenerator) {
                display.setCursor(0, 20);
                display.setTextColor(BLACK, WHITE); // 'inverted' text
                display.print("WA");
                display.setTextColor(WHITE);
            }
            if (functionGenerator2) {
                display.setCursor(32, 20);
                display.setTextColor(BLACK, WHITE); // 'inverted' text
                display.print("CA");
                display.setTextColor(WHITE);
            }
            if (functionGenerator3) {
                display.setCursor(0, 40);
                display.setTextColor(BLACK, WHITE); // 'inverted' text
                display.print("SI");
                display.setTextColor(WHITE);
            }
            if (functionGenerator4) {
                display.setCursor(32, 40);
                display.setTextColor(BLACK, WHITE); // 'inverted' text
                display.print("PU");
                display.setTextColor(WHITE);
            }
        }
    }
    display.display();
}

void handleFunctions() {
    if (functionGenerator) {
        if (millis() > (lastFunctionTime + 10)) { // step every 10 ms
            x++;
            if (x > 200) {
                x = 0;
            }
            int y = (sin(2 * x * x * PI / 10000) + 1) * 127 / 4 + 50;
            MIDI.sendControlChange(MIDI_CC_EXP1_PEDAL, y, MIDI_CHANNEL);
            lastFunctionTime = millis();
        }
    }
    if (functionGenerator2) {
        if (millis() >= (function2nextNoteTime)) {
            MIDI.sendControlChange(80, canon[canon_note_position][0], MIDI_CHANNEL); // Note
            MIDI.sendControlChange(81, canon[canon_note_position][1], MIDI_CHANNEL); // Octave

            function2nextNoteTime = millis() + canon[canon_note_position][2] * 60 * 1000 / canon_bpm;
            canon_note_position++;
            if (canon_note_position >= canon_number_of_notes) {
                canon_note_position = 0;
            }
        }
    }
    if (functionGenerator3) {
        if (millis() > (lastFunctionTime3 + 20)) { // step every 20 ms
            x2++;
            if (x2 > 100) {
                x2 = 0;
            }
            int y = (sin(2 * x2 * PI / 100) + 1) * 127 / 2;
            MIDI.sendControlChange(MIDI_CC_11, y, MIDI_CHANNEL);
            lastFunctionTime3 = millis();
        }
    }
    if (functionGenerator4) {
            if (millis() > (lastFunctionTime4 + 272)) { // step every 272 ms    60/110 bpm/2
                x4=!x4;
                MIDI.sendControlChange(MIDI_CC_FS1, (x4==0)?0:127, MIDI_CHANNEL);
                lastFunctionTime4 = millis();
            }
        }
}

void drawScreenForModelChange() {
    display.clearDisplay();
    display.setTextColor(WHITE);
    display.setCursor(0, 0);
    display.setTextSize(2);
    if (hxStompMode) {
        display.print("HX Stomp");
    } else {
        display.print("HX Floor");
    }
    display.display();
    long startTime = millis();
    while (millis() < startTime + 2000) {
        handleFunctions();
    }
    drawScreen();
}

void drawScreenForReset() {
    display.clearDisplay();
    display.setTextColor(WHITE);
    display.setCursor(0, 0);
    display.setTextSize(2);
    display.print("Resetting");
    display.display();
    delay(1000);
}

int getNumberOfPresetsForCurrentMode() {
    if (hxStompMode) {
        return PRESET_PER_BANK_STOMP;
    } else {
        return PRESET_PER_BANK_FLOOR;
    }
}

String getCurrentViewString() {
    switch (currentView) {
    case FS_VIEW:
        return "FS_VIEW";
        break;
    case SNAPSHOT_VIEW:
        return "SNAPSHOT_VIEW";
        break;
    case PRESET_VIEW:
        return "PRESET_VIEW";
        break;
    case SET_VIEW:
        return "SET_VIEW";
        break;
    case FUNCTION_VIEW:
        return "FUNCTION_VIEW";
        break;
    case LOOPER_VIEW:
        return "LOOPER_VIEW";
        break;
    case SPECIAL_VIEW:
        return "SPECIAL_VIEW";
        break;
    default:
        return "ERROR VIEW!";
        break;
    }
}

void readButtons() {
    viewButtonValue = digitalRead(VIEW_BUTTON);
    button1value = digitalRead(BUTTON1);
    button2value = digitalRead(BUTTON2);
    button3value = digitalRead(BUTTON3);
    button4value = digitalRead(BUTTON4);
    button5value = digitalRead(BUTTON5);
    button6value = digitalRead(BUTTON6);
}

void handleButton1() {
    if (button1valuePrev && !button1value) {
        viewButtonPressedTime = LONG_MAX;
        delay(DEBOUNCE_TIME);
        if (!digitalRead(BUTTON1)) { // Prevent bounce
            anyChangeMade = true;

            if (viewButtonValue) { // If view button not pressed
                switch (currentView) {
                case FS_VIEW:
                    Serial.println("Button1: FS1 toggle");
                    MIDI.sendControlChange(MIDI_CC_FS1, 127, MIDI_CHANNEL); // Any value will toggle
                    break;
                case SNAPSHOT_VIEW:
                    Serial.println("Button1: Snapshot 1");
                    MIDI.sendControlChange(MIDI_CC_SNAPSHOT, 0, MIDI_CHANNEL); // Snapshot Next
                    break;
                case PRESET_VIEW:
                    Serial.print("Button1: Preset +");

                    if (currentProgram >= 0) {
                        if (hxStompMode) {
                            currentProgram += PRESET_PER_BANK_STOMP;
                            Serial.print(PRESET_PER_BANK_STOMP);
                        } else {
                            currentProgram += PRESET_PER_BANK_FLOOR;
                            Serial.print(PRESET_PER_BANK_FLOOR);
                        }
                        if (currentProgram > 125) {
                            currentProgram = 125; // TODO: Handle floor max = 127
                            Serial.print(" stay at 125");
                        }
                    }
                    MIDI.sendProgramChange(currentProgram, MIDI_CHANNEL); // PC 0
                    Serial.print(" (");
                    Serial.print(currentProgram);
                    Serial.println(")");
                    break;

                case SET_VIEW:
                    Serial.println("Button1: Set");
                    MIDI.sendControlChange(MIDI_CC_BANK_LSB, 0, MIDI_CHANNEL);
                    MIDI.sendProgramChange(0, MIDI_CHANNEL); // PC 0
                    break;
                case FUNCTION_VIEW:
                    functionGenerator = !functionGenerator;
                    if (!functionGenerator) {
                        MIDI.sendControlChange(MIDI_CC_EXP1_PEDAL, 61, MIDI_CHANNEL);
                    }
                    Serial.print("Button1: Function ");
                    Serial.println(functionGenerator);
                    break;
                case LOOPER_VIEW:
                    Serial.print("Button1: Looper");
                    button1LooperState = !button1LooperState;
                    MIDI.sendControlChange(MIDI_CC_LOOPER_UNDO_REDO, 127, MIDI_CHANNEL); // UNDO/REDO
                    Serial.println(" Undo/Redo");
                    break;
                case SPECIAL_VIEW:
                    Serial.println("Special View + Button1: RESET");
                    drawScreenForReset();
                    resetFunc(); //call reset
                default:
                    // statements
                    break;
                }
            } else if (!viewButtonValue && !digitalRead(VIEW_BUTTON)) {
                Serial.println("Button View + Button1: FS view");
                currentView = FS_VIEW;
            }
        }
    }
}

void handleButton2() {
    if (button2valuePrev && !button2value) {
        viewButtonPressedTime = LONG_MAX;
        delay(DEBOUNCE_TIME);
        if (!digitalRead(BUTTON2)) { // Prevent bounce
            anyChangeMade = true;
            if (viewButtonValue) { // If view button not pressed
                switch (currentView) {
                case FS_VIEW:
                    Serial.println("Button2: FS2 toggle");
                    MIDI.sendControlChange(MIDI_CC_FS2, 127, MIDI_CHANNEL); // Any value will toggle
                    break;
                case SNAPSHOT_VIEW:
                    Serial.println("Button2: Snapshot 2");
                    MIDI.sendControlChange(MIDI_CC_SNAPSHOT, 1, MIDI_CHANNEL); // Snapshot Next
                    break;
                case PRESET_VIEW:
                    Serial.print("Button2: Preset +");
                    if ((!hxStompMode && currentProgram < 127) || (hxStompMode && currentProgram < 125)) {
                        MIDI.sendProgramChange(++currentProgram, MIDI_CHANNEL); // PC 0
                    } else {
                        Serial.print("currentProgram on max. Rolling over to 0.");
                        currentProgram = 0;
                        MIDI.sendProgramChange(currentProgram, MIDI_CHANNEL);
                    }
                    Serial.print("Button2: Preset + (");
                    Serial.print(currentProgram);
                    Serial.println(")");
                    // HX stomp 126 PRESETS 01A-42C
                    // FLOOR 01A-32D
                    break;
                case SET_VIEW:
                    Serial.println("Button2: Set2");
                    MIDI.sendControlChange(MIDI_CC_BANK_LSB, 2, MIDI_CHANNEL);
                    MIDI.sendProgramChange(0, MIDI_CHANNEL); // PC 0
                    break;
                case FUNCTION_VIEW:
                    functionGenerator2 = !functionGenerator2;
                    Serial.print("Button2: Function2 ");
                    Serial.println(functionGenerator2);
                    canon_note_position = 0;
                    function2nextNoteTime = millis();
                    MIDI.sendControlChange(MIDI_CC_FS1, 127, MIDI_CHANNEL); // Turn on off 3 note generator. Any value will toggle
                    break;
                case LOOPER_VIEW:
                    Serial.print("Button2: Looper");
                    MIDI.sendControlChange(MIDI_CC_LOOPER_PLAY_ONCE, 127, MIDI_CHANNEL); // Once
                    Serial.println(" Once");
                    break;
                default:
                    // statements
                    break;
                }
            } else if (!viewButtonValue && !digitalRead(VIEW_BUTTON)) {
                Serial.println("Button View + Button2: Snapshot view");
                currentView = SNAPSHOT_VIEW;
            }
        }
    }
}

void handleButton3() {
    if (button3valuePrev && !button3value) {
        viewButtonPressedTime = LONG_MAX;
        delay(DEBOUNCE_TIME);
        if (!digitalRead(BUTTON3)) { // Prevent bounce
            anyChangeMade = true;
            if (viewButtonValue) { // If view button not pressed
                switch (currentView) {
                case FS_VIEW:
                    Serial.println("Button3: FS2 toggle");
                    MIDI.sendControlChange(MIDI_CC_FS3, 127, MIDI_CHANNEL); // Any value will toggle
                    break;
                case SNAPSHOT_VIEW:
                    Serial.println("Button3: Snapshot 2");
                    MIDI.sendControlChange(MIDI_CC_SNAPSHOT, 2, MIDI_CHANNEL); // Snapshot Next
                    break;
                case PRESET_VIEW:
                    Serial.println("Button3: Preset 0");
                    MIDI.sendProgramChange(0, MIDI_CHANNEL); // PC 0
                    currentProgram = 0;
                    break;
                case SET_VIEW:
                    Serial.println("Button3: Set3");
                    MIDI.sendControlChange(MIDI_CC_BANK_LSB, 3, MIDI_CHANNEL);
                    MIDI.sendProgramChange(0, MIDI_CHANNEL); // PC 0
                    break;
                case FUNCTION_VIEW:
                    Serial.print("Button3: Function3 ");
                    Serial.println("Tuner");
                    MIDI.sendControlChange(MIDI_CC_TUNER_SCREEN, 0, MIDI_CHANNEL); // Tuner on/off

                    break;
                case LOOPER_VIEW:
                    Serial.print("Button3: Looper");

                    button3looperState = !button3looperState;
                    if (button3looperState) {
                        MIDI.sendControlChange(MIDI_CC_LOOPER_FULL_HALF, 127, MIDI_CHANNEL); // HALF
                        Serial.println(" Half");
                    } else {
                        MIDI.sendControlChange(MIDI_CC_LOOPER_FULL_HALF, 0, MIDI_CHANNEL); // FULL
                        Serial.println(" Full");
                    }

                    break;
                case SPECIAL_VIEW:
                    Serial.println("Special View + Button3: Return");
                    currentView = FS_VIEW;
                    break;
                default:
                    // statements
                    break;
                }
            } else if (!viewButtonValue && !digitalRead(VIEW_BUTTON)) {
                Serial.println("Button View + Button3: Preset view");
                currentView = PRESET_VIEW;
            }
        }
    }
}

void handleButton4() {
    if (button4valuePrev && !button4value) {
        viewButtonPressedTime = LONG_MAX;
        delay(DEBOUNCE_TIME);
        if (!digitalRead(BUTTON4)) { // Prevent bounce
            anyChangeMade = true;
            if (viewButtonValue) { // If view button not pressed
                switch (currentView) {
                case FS_VIEW:
                    Serial.println("Button4: FS4 toggle");
                    MIDI.sendControlChange(MIDI_CC_FS4, 127, MIDI_CHANNEL); // Any value will toggle
                    break;
                case SNAPSHOT_VIEW:
                    if (!hxStompMode) {
                        Serial.println("Button4: Snapshot 4");
                        MIDI.sendControlChange(MIDI_CC_SNAPSHOT, 3, MIDI_CHANNEL); // Snapshot Next
                    } else {
                        Serial.println("Button4: FS1 toggle");
                        MIDI.sendControlChange(MIDI_CC_FS1, 127, MIDI_CHANNEL); // Any value will toggle
                    }
                    break;
                case PRESET_VIEW:
                    Serial.print("Button4: Preset -");
                    if (currentProgram >= getNumberOfPresetsForCurrentMode()) {
                        if (hxStompMode) {
                            currentProgram -= PRESET_PER_BANK_STOMP;
                            Serial.print(PRESET_PER_BANK_STOMP);
                        } else {
                            currentProgram -= PRESET_PER_BANK_FLOOR;
                            Serial.print(PRESET_PER_BANK_FLOOR);
                        }
                        if (currentProgram < 0) {
                            currentProgram = 0;
                            Serial.print(" stay at 0");
                        }
                    }
                    MIDI.sendProgramChange(currentProgram, MIDI_CHANNEL); // PC 0
                    Serial.print(" (");
                    Serial.print(currentProgram);
                    Serial.println(")");
                    break;
                case SET_VIEW:
                    Serial.println("Button4: Set4");
                    MIDI.sendControlChange(MIDI_CC_BANK_LSB, 4, MIDI_CHANNEL);
                    MIDI.sendProgramChange(0, MIDI_CHANNEL); // PC 0
                    break;
                case FUNCTION_VIEW:
                    functionGenerator3 = !functionGenerator3;
                    if (!functionGenerator3) {
                        MIDI.sendControlChange(MIDI_CC_11, 61, MIDI_CHANNEL);
                    }
                    Serial.print("Button4: Function ");
                    Serial.println(functionGenerator3);
                    break;
                case LOOPER_VIEW:
                    Serial.print("Button4: Looper");
                    button4looperState = !button4looperState;
                    if (button4looperState) {
                        MIDI.sendControlChange(MIDI_CC_LOOPER_RECORD_OVERDUB, 127, MIDI_CHANNEL); // RECORD
                        Serial.println(" Record");
                    } else {
                        MIDI.sendControlChange(MIDI_CC_LOOPER_RECORD_OVERDUB, 0, MIDI_CHANNEL); // OVERDUB
                        Serial.println(" Overdub");
                    }

                    break;
                default:
                    // statements
                    break;
                }
            } else if (!viewButtonValue && !digitalRead(VIEW_BUTTON)) {
                if (hxStompMode) {
                    Serial.println("Button View + Button4: Hx stomp preset view");
                    currentView = PRESET_VIEW;
                } else {
                    Serial.println("Button View + Button4: Set view");
                    currentView = SET_VIEW;
                }
            }
        }
    }
}

void handleButton5() {
    if (button5valuePrev && !button5value) {
        viewButtonPressedTime = LONG_MAX;
        delay(DEBOUNCE_TIME);
        if (!digitalRead(BUTTON5)) { // Prevent bounce
            anyChangeMade = true;
            if (viewButtonValue) { // If view button not pressed
                switch (currentView) {
                case FS_VIEW:
                    Serial.println("Button5: FS5 toggle");
                    MIDI.sendControlChange(MIDI_CC_FS5, 127, MIDI_CHANNEL); // Any value will toggle
                    break;
                case SNAPSHOT_VIEW:
                    if (!hxStompMode) {
                        Serial.println("Button5: Snapshot 5");
                        MIDI.sendControlChange(MIDI_CC_SNAPSHOT, 4, MIDI_CHANNEL);
                    } else {
                        Serial.println("Button5: FS2 toggle");
                        MIDI.sendControlChange(MIDI_CC_FS2, 127, MIDI_CHANNEL); // Any value will toggle
                    }
                    break;
                case PRESET_VIEW:
                    Serial.print("Button5: Preset - (");
                    if (currentProgram > 0) {
                        MIDI.sendProgramChange(--currentProgram, MIDI_CHANNEL); // PC -
                        Serial.print(currentProgram);
                    } else {
                        Serial.print("stay at 0");
                    }
                    Serial.println(")");
                    break;
                case SET_VIEW:
                    Serial.println("Button5: Set5");
                    MIDI.sendControlChange(MIDI_CC_BANK_LSB, 5, MIDI_CHANNEL);
                    MIDI.sendProgramChange(0, MIDI_CHANNEL); // PC 0
                    break;
                case FUNCTION_VIEW:
                    functionGenerator4 = !functionGenerator4;
                    if (!functionGenerator4) {
                        MIDI.sendControlChange(MIDI_CC_FS1, 0, MIDI_CHANNEL);
                    }
                    Serial.print("Button5: Function ");
                    Serial.println(functionGenerator4);
                    break;
                case LOOPER_VIEW:
                    Serial.print("Button5: Looper");
                    button5looperState = !button5looperState;
                    if (button5looperState) {
                        MIDI.sendControlChange(MIDI_CC_LOOPER_STOP_PLAY,
                        MIDI_LOOPER_PLAY, MIDI_CHANNEL); // PLAY
                        Serial.println(" Play");
                    } else {
                        MIDI.sendControlChange(MIDI_CC_LOOPER_STOP_PLAY,
                        MIDI_LOOPER_STOP, MIDI_CHANNEL); // STOP
                        Serial.println(" Stop");
                    }
                    break;
                default:
                    // statements
                    break;
                }
            } else if (!viewButtonValue && !digitalRead(VIEW_BUTTON)) {
                Serial.println("Button View + Button5: Function view");
                currentView = FUNCTION_VIEW;
            }
        }
    }
}

void handleButton6() {
    if (button6valuePrev && !button6value) {
        viewButtonPressedTime = LONG_MAX;
        delay(DEBOUNCE_TIME);
        if (!digitalRead(BUTTON6)) { // Prevent bounce
            anyChangeMade = true;
            if (viewButtonValue) { // If view button not pressed
                switch (currentView) {
                case FS_VIEW:
                    if (!hxStompMode) {
                        Serial.println("Button6: FS7 toggle");
                        MIDI.sendControlChange(MIDI_CC_FS7, 127, MIDI_CHANNEL); // Any value will toggle
                    } else {
                        Serial.println("Button6: FS7 not change as we are in HX stomp mode");
                    }
                    break;
                case SNAPSHOT_VIEW:
                    if (!hxStompMode) {
                        Serial.println("Button6: Snapshot 6");
                        MIDI.sendControlChange(MIDI_CC_SNAPSHOT, 5, MIDI_CHANNEL); // Snapshot Next
                    } else {
                        Serial.println("Button6: FS3 toggle");
                        MIDI.sendControlChange(MIDI_CC_FS3, 127, MIDI_CHANNEL); // Any value will toggle
                    }
                    break;
                case PRESET_VIEW:
                    Serial.print("Button6: Preset (");
                    MIDI.sendProgramChange(++currentProgram, MIDI_CHANNEL); // PC 0
                    Serial.print(currentProgram);
                    Serial.print(")");
                    break;
                case SET_VIEW:
                    Serial.println("Button6: Set6");
                    MIDI.sendControlChange(MIDI_CC_BANK_LSB, 6, MIDI_CHANNEL);
                    MIDI.sendProgramChange(0, MIDI_CHANNEL); // PC 0
                    break;
                case FUNCTION_VIEW:
                    Serial.println("Button6: Tap tempo");
                    MIDI.sendControlChange(MIDI_CC_TAP_TEMPO, 127, MIDI_CHANNEL);
                    break;
                case LOOPER_VIEW:
                    Serial.print("Button2: Looper");
                    button6looperState = !button6looperState;
                    if (button6looperState) {
                        MIDI.sendControlChange(MIDI_CC_LOOPER_FORW_REV, 127, MIDI_CHANNEL); // Reverse
                        Serial.println(" Play");
                    } else {
                        MIDI.sendControlChange(MIDI_CC_LOOPER_FORW_REV, 0, MIDI_CHANNEL); // Forward
                        Serial.println(" Stop");
                    }
                    break;
                case SPECIAL_VIEW:
                    Serial.println("Special View + Button6: Toggle stomp mode");
                    hxStompMode = !hxStompMode;
                    drawScreenForModelChange();
                    currentView = FS_VIEW;
                    break;
                default:
                    // statements
                    break;
                }
            } else if (!viewButtonValue && !digitalRead(VIEW_BUTTON)) {
                Serial.println("Button View + Button6: Looper view");
                currentView = LOOPER_VIEW;
            }
        }
    }
}

void handleViewButton() {
    if (viewButtonPrev && !viewButtonValue) { // Only trigger when off to on
        viewButtonPressedTime = millis();
        delay(DEBOUNCE_TIME);
        if (!digitalRead(VIEW_BUTTON)) { // Prevent bounce
            anyChangeMade = true;
            currentView++;
            if (currentView >= NUMBER_OF_VIEWS) {
                currentView = 0;
            }

            if (currentView == SET_VIEW && hxStompMode) {
                currentView++; // Skip set view for stomp
            }

            Serial.print("Button View: ");
            Serial.println(getCurrentViewString());
        }
    }
    if (!viewButtonPrev && !viewButtonValue) {
        // View button still pressed
        if (millis() > (viewButtonPressedTime + 3000)) {
            Serial.println("View button held 3s");
            anyChangeMade = true;
            currentView = SPECIAL_VIEW;
        }
    }
}

void storeButtonValues() {
    viewButtonPrev = viewButtonValue;
    button1valuePrev = button1value;
    button2valuePrev = button2value;
    button3valuePrev = button3value;
    button4valuePrev = button4value;
    button5valuePrev = button5value;
    button6valuePrev = button6value;
    wasConnected = isConnected;
}

// ====================================================================================
// Event handlers for incoming MIDI messages
// ====================================================================================

// -----------------------------------------------------------------------------
// Device connected
// -----------------------------------------------------------------------------
void OnConnected() {
    isConnected = true;
    digitalWrite(LED_BUILTIN, HIGH);
    Serial.println("Connected");
}

// -----------------------------------------------------------------------------
// Device disconnected
// -----------------------------------------------------------------------------
void OnDisconnected() {
    isConnected = false;
    digitalWrite(LED_BUILTIN, LOW);
    Serial.println("Disconnected");
}

// -----------------------------------------------------------------------------
// Received note on
// -----------------------------------------------------------------------------
void OnNoteOn(byte channel, byte note, byte velocity) {
    Serial.println("NoteOn");
}

// -----------------------------------------------------------------------------
// Received note off
// -----------------------------------------------------------------------------
void OnNoteOff(byte channel, byte note, byte velocity) {
    Serial.println("NoteOff");
}

void OnProgramChange(byte channel, byte number) {
    Serial.print("OnProgramChange, channel:");
    Serial.print(channel);
    Serial.print(" ,number: ");
    Serial.println(number);
    currentProgram = number;
    anyChangeMade = true;
    if (controlChangeReceived == true) {
        hxStompMode = false;
    } else {
        hxStompMode = true;
    }
}

void OnControlChange(byte channel, byte number, byte value) {
    controlChangeReceived = true;
    Serial.print("OnControlChange, channel:");
    Serial.print(channel);
    Serial.print(" ,number: ");
    Serial.print(number);
    Serial.print(" ,value: ");
    Serial.println(value);
    anyChangeMade = true;
}

void OnClock() {
    Serial.println("OnClock");
}

void OnStart() {
    Serial.println("OnStart");
}

void OnStop() {
    Serial.println("OnStop");
}

void OnContinue() {
    Serial.println("OnContinue");
}

void OnActiveSensing() {
    Serial.println("OnActiveSensing");
}

void OnSystemReset() {
    Serial.println("OnSystemReset");
}

void OnTuneRequest() {
    Serial.println("OnTuneRequest");
}

void OnAfterTouchPoly(byte channel, byte note, byte pressure) {
    Serial.println("OnAfterTouchPoly");
}

void OnAfterTouchChannel(byte channel, byte pressure) {
    Serial.println("OnAfterTouchChannel");
}

void OnPitchBend(byte channel, int bend) {
    Serial.println("OnPitchBend");
}

void OnSystemExclusive(byte *array, unsigned size) {
    Serial.println("OnSystemExclusive");
}

void OnTimeCodeQuarterFrame(byte data) {
    Serial.println("OnTimeCodeQuarterFramestemExclusive");
}

void OnSongPosition(unsigned int beats) {
    Serial.println("OnSongPosition");
}

void OnSongSelect(byte songnumber) {
    Serial.println("OnSongSelect");
}

// -----------------------------------------------------------------------------
// LOOP
// -----------------------------------------------------------------------------
void loop() {
    anyChangeMade = false;
    readButtons();
    MIDI.read();
    handleViewButton();
    handleButton1();
    handleButton2();
    handleButton3();
    handleButton4();
    handleButton5();
    handleButton6();
    handleFunctions();

    if (anyChangeMade || (wasConnected == false && isConnected == true)) {
        drawScreen();
    }
    storeButtonValues();
}
