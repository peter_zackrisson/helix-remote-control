/*
 * Midi definitions Line 6 Helix
 *
 * Floor: https://line6.com/data/6/0a020a4112b165fb6a3fe41663/application/pdf/Helix%203.0%20Owner's%20Manual%20-%20Rev%20E%20-%20English%20.pdf
 * Stomp: https://line6.com/data/6/0a020a4112bbf5fb6a519e8b18/application/pdf/HX%20Stomp%203.0%20Owner's%20Manual%20-%20Rev%20C%20-%20English%20.pdf
 */

#define MIDI_CC_EXP1_PEDAL 1                              // Stomp + Floor
#define MIDI_CC_EXP2_PEDAL 2                              // Stomp + Floor
#define MIDI_CC_EXP3_PEDAL 3                              // Floor
#define MIDI_CC_FS1 49                                    // Stomp + Floor
#define MIDI_CC_FS2 50                                    // Stomp + Floor
#define MIDI_CC_FS3 51                                    // Stomp + Floor
#define MIDI_CC_FS4 52                                    // Stomp + Floor
#define MIDI_CC_FS5 53                                    // Stomp + Floor
#define MIDI_CC_FS7 54                                    // Floor
#define MIDI_CC_FS8 55                                    // Floor
#define MIDI_CC_FS9 56                                    // Floor
#define MIDI_CC_FS10 57                                   // Floor
#define MIDI_CC_FS11 58                                   // Floor
#define MIDI_CC_EXP_TOE 59                                // Floor

#define MIDI_CC_11 11
#define MIDI_CC_12 12

#define MIDI_CC_LOOPER_RECORD_OVERDUB 60                  // Stomp + Floor (FS8) 0-63 Overdub, 64-127: Record
#define MIDI_CC_LOOPER_STOP_PLAY 61                       // Stomp + Floor (FS9) 0-63 Stop, 64-127: Play
#define MIDI_LOOPER_PLAY 127
#define MIDI_LOOPER_STOP 0

#define MIDI_CC_LOOPER_PLAY_ONCE 62                       // Stomp + Floor (FS3) Play Once
#define MIDI_CC_LOOPER_UNDO_REDO 63                       // Stomp + Floor (FS2)
#define MIDI_CC_LOOPER_FORW_REV 65                        // Stomp + Floor (FS11) 0-63 Forward, 64-127: Reverse
#define MIDI_CC_LOOPER_FULL_HALF 66                       // FS10 Stomp + Floor (FS10) 0-63 Full, 64-127: Half
#define MIDI_CC_LOOPER_OFF_ON 67                          // Stomp + Floor 0-63 on/Off also endters/exits looper fotswitch mode, 64-127: On

#define MIDI_CC_BANK_MSB 0                                // Floor 0-7
#define MIDI_CC_BANK_LSB 32                               // Floor 0-7 Setlist Select

#define MIDI_CC_TAP_TEMPO 64                              // Stomp + Floor 64-127
#define MIDI_CC_TUNER_SCREEN 68                           // Stomp + Floor 0-127

#define MIDI_CC_SNAPSHOT 69                               // Floor 0-7 Snapshot 1-8, Stomp 0-2 Snapshot 1-3, 8: Next Snapshot, 9: Previous Snapshot
#define MIDI_SNAPSHOT_NEXT 8
#define MIDI_SNAPSHOT_PREV 9

#define MIDI_CC_STOMP_ALL_BYPASS 70                       // Stomp 0-63: Bypass, 64-127: On

#define MIDI_CC_STOMP_FOOTSWITCH_MODE 71                  // Stomp 0-5: 0=Stomp, 1=Scroll, 2=Preset, 3=Snapshot, 4=Next footswitch mode, 5=Previous footswitch mode
#define MIDI_STOMP_FOOTSWITCH_MODE_STOMP 0                // Stomp 0=Stomp
#define MIDI_STOMP_FOOTSWITCH_MODE_SCROLL 1               // Stomp 1=Scroll
#define MIDI_STOMP_FOOTSWITCH_MODE_PRESET 2               // Stomp 2=Preset
#define MIDI_STOMP_FOOTSWITCH_MODE_SNAPSHOT 3             // Stomp 3=Snapshot
#define MIDI_STOMP_FOOTSWITCH_MODE_NEXT_FOOTSWITCH_MODE 4 // Stomp 4=Next footswitch mode
#define MIDI_STOMP_FOOTSWITCH_MODE_PREV_FOOTSWITCH_MODE 5 // Stomp 5=Previous footswitch mode

#define MIDI_3_NOTE_OSCILLATOR_C        0
#define MIDI_3_NOTE_OSCILLATOR_Csharp  12
#define MIDI_3_NOTE_OSCILLATOR_D       24
#define MIDI_3_NOTE_OSCILLATOR_Dsharp  35
#define MIDI_3_NOTE_OSCILLATOR_E       47
#define MIDI_3_NOTE_OSCILLATOR_F       58
#define MIDI_3_NOTE_OSCILLATOR_Fsharp  72
#define MIDI_3_NOTE_OSCILLATOR_G       87
#define MIDI_3_NOTE_OSCILLATOR_Gsharp  97
#define MIDI_3_NOTE_OSCILLATOR_A      105
#define MIDI_3_NOTE_OSCILLATOR_Asharp 119
#define MIDI_3_NOTE_OSCILLATOR_B      127

#define MIDI_3_NOTE_OSCILLATOR_OCTAVE_0    0
#define MIDI_3_NOTE_OSCILLATOR_OCTAVE_1   16
#define MIDI_3_NOTE_OSCILLATOR_OCTAVE_2   32
#define MIDI_3_NOTE_OSCILLATOR_OCTAVE_3   48
#define MIDI_3_NOTE_OSCILLATOR_OCTAVE_4   64
#define MIDI_3_NOTE_OSCILLATOR_OCTAVE_5   80
#define MIDI_3_NOTE_OSCILLATOR_OCTAVE_6   96
#define MIDI_3_NOTE_OSCILLATOR_OCTAVE_7  112

#define HALF_NOTE 2
#define FULL_NOTE 4
#define QUARTER_NOTE 1
